## Introduction

A simple program to generate syntehtic images of Ag nanocubes intended to resemble images created through scanning electron microscopy (SEM).

## Building

### Dependencies

* C++ 17 (needed for filesystem header)
* OpenCV 4.5.X

## License
This program is licensed under the terms of the version 3 of the GNU General Public License. See the LICENSE.txt file for more details.
