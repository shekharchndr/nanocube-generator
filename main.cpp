/**********************************************************************
//Copyright (c) 2021-2022 Jorge López
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//See LICENSE.txt file for details
**********************************************************************/

#include <iostream>
#include <fstream>
#include <ctime>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/stitching/detail/util.hpp>

using namespace cv;
using namespace std;

struct config{
	int width;
	int height;
	vector<string> filelist;
};

config loadPatternFile(string);
void printConfig(config &);
Mat getTemplateImg(vector<string> &);
bool withinBounds(Point, Size, Size);
Point getNewPosition(Size);
bool regionAvailable(Rect &, Rect &);

config loadPatternFile(string filename)
{
	config patternconfig;
	string line;

	ifstream pfile_ifs(filename.c_str());
	if (!pfile_ifs.is_open())
		{
			cout << "File " + filename + " not found" << endl;
			return patternconfig;
		}
	
	while (getline(pfile_ifs, line))
		{
			if (line.find("X") != string::npos)
				{
					size_t seppos = line.find("X");
					patternconfig.width = stoi(line.substr(0,seppos));
					patternconfig.height = stoi(line.substr(seppos+1));
				}
			if (line.find(".png") != string::npos)
				{
					patternconfig.filelist.push_back(line);
				}
		}
	pfile_ifs.close();

	return patternconfig;
}

Mat getTemplateImg(vector<string> & filelist)
{
	int pos = (rand() % filelist.size());

	//cout << "loading template: " << filelist.at(pos) << endl;

	Mat tpl_img = imread(filelist.at(pos), IMREAD_COLOR);
	/*if (cube_tpl_img.empty())
		{
			cout << "Cannot open image!" << endl;
    }*/
	return tpl_img;
}

bool withinBounds(Point pos, Size fore_dims, Size bkg_dims)
{
	int width = pos.x + fore_dims.width, height = pos.y + fore_dims.height;

	if (pos.x > 0 && width < bkg_dims.width &&
			pos.y > 0 && height < bkg_dims.height)
		return true;

	return false;
}

void printConfig(config & cfg)
{
	cout << "Width: " << cfg.width
			 << "\nHeight: " << cfg.height
			 << "\nTemplates: " << cfg.filelist.size() << endl;
}

int getRotation()
{
	return RotateFlags((rand() % 3));
}

Point getNewPosition(Size range)
{
	Point p;
	
	p.x = (rand() % range.width);
	p.y = (rand() % range.height);

	return p;
}

bool regionAvailable(Rect & roi, vector<Rect> & regions)
{
	if (regions.empty()) return true;

	for(Rect region : regions)
			if (cv::detail::overlapRoi(roi.tl(),region.tl(),roi.size(),region.size(),roi))
				return false;
	
	return true;
}

vector<Rect> unavailable_regions;

int main(int argc, char *argv[])
{
	if (argc < 3)
		{
			cout << "Missing arguments." << endl;
			return -1;
		}

	config cfg = loadPatternFile(argv[1]);
	printConfig(cfg);
	const unsigned short NP_COUNT = stoi(argv[2]);

	time_t timer = time(0);
	//time(&timer);
	tm *ltime = localtime(&timer);
	string tmstamp = "_" + to_string(ltime->tm_mday)
		+ to_string(1+ltime->tm_mon)
		+ to_string(1900 + ltime->tm_year)
		+ "_"
		+ to_string(ltime->tm_hour)
		+ to_string(ltime->tm_min)
		+ to_string(ltime->tm_sec);

	Mat final_img = Mat::zeros(cfg.height,cfg.width,CV_8UC3);
	Point pos;
	Mat tpl_img,np_img;
	Rect np_region;
	int rot_flag;
	unsigned short int FINAL_NP_COUNT = 0;

	srand(time(NULL));

	for(int i = 0; i < NP_COUNT; i++)
		{
			tpl_img = getTemplateImg(cfg.filelist);
			Size tpl_dims = tpl_img.size();
			rot_flag = getRotation();
			rotate(tpl_img,np_img,ROTATE_90_CLOCKWISE);
			
			pos = getNewPosition(final_img.size());
						
			if (withinBounds(pos,np_img.size(),final_img.size()))
				{
					np_region = Rect(pos.x, pos.y, np_img.cols, np_img.rows);
					//cout << "new np region "<< np_region << endl;
					if (regionAvailable(np_region, unavailable_regions))
						{
							//cout << "available" << endl;
							np_img.copyTo(final_img(np_region));
							unavailable_regions.push_back(np_region);
							FINAL_NP_COUNT++;
						}
				}
		}

	string oimg_name = "AgCube_" + to_string(FINAL_NP_COUNT) + "NP" + tmstamp + ".jpg";
	//namedWindow(oimg_name);imshow(oimg_name, final_img);waitKey(0);
	imwrite(oimg_name, final_img);
	cout << "Image generated with " << FINAL_NP_COUNT
			 << " NPs and saved as " << oimg_name << endl;

	return 0;
}
